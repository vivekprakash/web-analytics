var xmlHttp

function submitStatus(username,resp,sid,pVal)
{
    if (resp=="")
    	resp="NA";
    xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
	  {
	    alert ("Your browser does not support AJAX!");
	      return;
	  } 

		var url="submit_status.php";
		url=url+"?username="+username+"&status="+resp+"&sessionid="+sid;
		url=url+"&sid="+Math.random();
		url=url+"&pVal="+pVal;
		xmlHttp.onreadystatechange=stateChanged;
		xmlHttp.open("GET",url,true);
		xmlHttp.send(null);
		} 

		function stateChanged() 
		{ 
		if (xmlHttp.readyState==4)
		{ 
		}
		}

		function GetXmlHttpObject()
		{
		var xmlHttp=null;
		try
		  {
		    // Firefox, Opera 8.0+, Safari
		       xmlHttp=new XMLHttpRequest();
		   }
		  catch (e)
		   {
		       // Internet Explorer
		         try
		         {
		                xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		          }
		          catch (e)
		        {
		               xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		           }
		     }
		     return xmlHttp;
		}




function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
			document.cookie = name+"="+value+expires+"; path=/";
		}

function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
																			return null;
																			}

function showSmileys() {
	document.getElementById('smileys').style.display = "block";
	document.getElementById('status_msg').focus();

	setTimeout('check("bool", "smileys")',1000);
}
function check(boolVal, objVal)
{
	if(document.getElementById(boolVal).value==0)
		document.getElementById(objVal).style.display="none";
	else {
		if (objVal == "smileys")
			showSmileys();
		else
			showPref();
	}

}


function includeSmiley(smType) {
	document.getElementById('status_msg').focus();
	document.getElementById('status_msg').value += smType;
	document.getElementById('smileys').style.display = "none";
}
function hideSmileys() {
	document.getElementById('smileys').style.display = "none";
}
function changeColor(smType, colorVal) {
	smType.style.backgroundColor = colorVal;
}
function showPref() {
        document.getElementById('pref').style.display = "block";
	setTimeout('check("bool1", "pref")',1000);
}
function changePref(objVal, pV) {
	document.getElementById('pref1').innerHTML = "Show Status Msg...";
	document.getElementById('pref2').innerHTML = "Show Current Track...";
	document.getElementById('status_msg').focus();
	document.getElementById('prefVal').value=pV;
	document.getElementById('status_msg').blur();
	document.getElementById(objVal).innerHTML = "&#10003;&nbsp;" + document.getElementById(objVal).innerHTML;
}
function prepareOpen(userId, frndId, sid, frndTitle) {
	if (userId != frndId) {
		if(parent.dhtmlwindow.netwinorder < 6) {
			var isFrndNull = false;
			if (window.parent.document.getElementById(frndId) == null)
				isFrndNull = true;
			parent.dhtmlwindow.open(frndId, "iframe", "chatfiles/external.php?&user="+userId+"&frnd="+frndId+"&sid="+sid, frndTitle, "width=220px,height=261px,resize=0,scrolling=1,center=0", "recal");
			if (isFrndNull == true)
				parent.dhtmlwindow.netwinorder++;
		}
		else 
			alert("You cannot chat with more than 6 users at a time.");
	}
}
