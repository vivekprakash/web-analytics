
<meta http-equiv="refresh" content="60">
<style type="text/css">
<!--
body{
	font-family:verdana;
	font-weight:900;
	font-size:12px;
	margin:0px;
	padding:0px;
	scrollbar-face-color:#f1f1f1;
	scrollbar-base-color:#f1f1f1;
	scrollbar-arrow-color:black;
	scrollbar-track-color:#F3F3F3;
	scrollbar-shadow-color:#f1f1f1;
	scrollbar-highlight-color:#f1f1f1;
	scrollbar-3dlight-color:#cccccc;
	scrollbar-darkshadow-Color:#cccccc;
}
#text{

	font-family:verdana;
	font-size:11px;
	border-bottom:1px groove #444444;
	float:left;
	width:100%;
	cursor:pointer;
	font-weight:normal;

}

h4{
font-size:14px;
color: #e75200;
font-family:arial,verdana;
padding-left:5px;
line-height:.3em;
}


-->
</style>

<script language="javascript" src="status.js"></script>
<?php
/*****************************
Purpose: Shows online users, generates daily usage report

***************************/


include("../../connection.php");
if (isset($_GET["user"]))
	$user=$_GET["user"];
if (isset($_GET["sessionid"]))
	$sid=$_GET["sessionid"];


$app_id=$_GET["app_id"];
$mode=$_GET["mode"];
$del=0;
if(isset($_GET["del"]))
	$del=$_GET["del"];

if (!get_magic_quotes_gpc()) {
    $user = addslashes($user);
    $app_id=addslashes($app_id);
} 

function get_smiley_list()
{
	$smileys=scandir("smileys");
	$arr=array();
	foreach($smileys as $sml)
	{
		$check=strpos($sml,"gif");
		if($check)
		{	$arr[]=$sml;

		}
	}
	return $arr;


}

$smiley_arr=get_smiley_list();

//echo "<body><h4>Online Users</h4>";

function detect_smiley($status)
{
	$base_url="smileys/";
	$img_src="<img src='$base_url{}' style='margin-bottom:-2px' style='height:16px; width:auto;'>";
	global $smiley_arr;	
	for($i=0;$i<sizeof($smiley_arr);$i++)
	{
		$sm_pcs=explode(".",$smiley_arr[$i]);
		$sm=$sm_pcs[0];
		$img=str_replace("{}",$smiley_arr[$i],$img_src);
		$status=str_replace($sm,$img,$status);

	}
	return $status;	

}




function sanitize($status,$flag)
{
	
	if ($flag==1)
	{
		if(strlen($status)>23)
			$status=substr($status,0,23)."...";
		$status=str_replace("'","&#39;",$status);
		$html_pat="/<[a-zA-Z\/][^>]*>/";
		$replace="";
		$status=preg_replace($html_pat,$replace,$status);
		$status = detect_smiley($status);
		return $status;
	}
	else{

		$html_pat="/<[a-zA-Z\/][^>]*>/";
		$replace="";
		$status=str_replace("'","&#39;",$status);
		$alt_status=preg_replace($html_pat,$replace,$status);
		return $alt_status;
	}
	
}	




function make_log($dbcon,$user,$app_id)
{
	if(trim($user)!="")
	{
	
		$query_already="delete from online_users where username='$user-$app_id' and app_id='$app_id'";
		pg_query($dbcon,$query_already);
		$current_time=time();

			
		$query_insert="insert into online_users (username,app_id,timest) values('$user-$app_id','$app_id','$current_time')";
		pg_query($dbcon,$query_insert);
	}
}

function get_online_users($app_id,$dbcon,$dbRj,$user,$sid)
{
	global $smiley_arr;
	include('checkCommonLogin.php');

	if(isLogin($user,$sid,$dbcon)==0)
		return;

	$current_time=time();
	$expire_time=$current_time-180;
	$song_expire_time=$current_time-400;
	

	$query_select="select username from online_users  where app_id='$app_id' and  timest>$expire_time order by timest ";
	$select_result=pg_query($dbcon,$query_select);
	$users_cnt=pg_num_rows($select_result);
	
	
//	echo "$users_cnt user(s) online<hr/>";

	$isSong=array();
	$sorted_names = array();
	while($row=pg_fetch_row($select_result))
	{
		
	
		
		$only_user=explode("-",$row[0]);
	

		$query_name="select name from person_view where person_id='$only_user[0]'";
		$name_result=pg_query($dbRj,$query_name);
		$name=pg_fetch_row($name_result);
		$name[0]=trim($name[0], ".");
		$name[0]=trim($name[0]);
		array_push($sorted_names,$name[0]);
		
		/***************Adding currently played song/Status Msg*********************/

		//if no possible status message found see if a song is playing
		

		$query_status="select status, preference from online_status where username='$only_user[0]'"; 
		$status_result=pg_query($dbcon,$query_status);
		$status=pg_fetch_row($status_result);
		if ($status==false)
			$status="NA";
		else {
			$preference = $status[1];
			$status = $status[0];
		}

	if ($only_user[0]==$user)
		{
			if ($status=="NA")
				$temp_status="Set status here";
			else
				$temp_status=$status;
			
?>
<div style="width:145px; height:auto; z-index:5; position:absolute;">
<input type="hidden" id="bool" />
<input type="hidden" id="bool1" />
<input type="hidden" id="prefVal" value="<? if ($status!="NA") echo $preference; else echo "1";?>" />
<?php        for($i=0;$i<sizeof($smiley_arr);$i++)
	        {
		 $sm=explode(".",$smiley_arr[$i]);
		echo "<div style='float:left;'><img src='smileys/$smiley_arr[$i]' style='height:16px; cursor:pointer; width:auto' onclick='includeSmiley(\"$sm[0]\")' onmouseover='changeColor(this, \"#ccc\")' onmouseout='changeColor(this, \"#fff\")' /></div>";
		}
?>
</div></div>
<div id="pref" style="width:116px; background-color:#fff; border:1px #ccc solid; position:absolute; z-index:5; height:auto; font-size:10px; font-family:Arial; margin-top:15px; display:none; font-weight:normal;" onmouseover="document.getElementById('bool1').value=1;" onmouseout="document.getElementById('bool1').value=0;">
<div style="cursor:pointer; border-bottom:1px #ccc solid;" id="pref1" onmouseover="changeColor(this, '#efefef')" onmouseout="changeColor(this, '#fff')" onclick="changePref('pref1', 1)"><?if($preference=="1") echo "&#10003;&nbsp;";?>Show Status Msg...</div>
<div style="cursor:pointer;" id="pref2" onmouseover="changeColor(this, '#efefef')" onmouseout="changeColor(this, '#fff')" onclick="changePref('pref2', 2)"><?if($preference=="2") echo "&#10003;&nbsp;";?>Show Current Track...</div>
</div>
<div style="margin-top:25px;">
<?

		}
		
		$isSong[$name[0]]=0;
		
		if($status=="NA" || $preference=="2")
		{
				
			$query_curr_song="select song_name from jukebox_curr_song where userid='$only_user[0]' and timest>$song_expire_time"; 
			$song_result=pg_query($dbcon,$query_curr_song);
			$song=pg_fetch_row($song_result);
			$song_status=$song[0];
			if ($song_status!="") {
				$status=$song_status;
				$isSong[$name[0]]=1;
			}
			else
				$isSong[$name[0]]=0;
		}
		$status_arr[$name[0]]=$status;
		/*****************************************************************/
	}
	sort($sorted_names);
	$i=0;
	while($i<$users_cnt)
	{
		echo "<span id='text' style='text-transform:uppercase' title='$sorted_names[$i]'>".substr($sorted_names[$i],0,15);
		if(strlen($sorted_names[$i])>15)
	                echo "...";
		if($status_arr[$sorted_names[$i]]!="" && $status_arr[$sorted_names[$i]]!="NA")
		{
			if($isSong[$sorted_names[$i]]==1)
				echo "<br/><marquee scrollamount='2'><span style='font-size:10px;color:#00941c;text-transform:capitalize'><img src='note.gif' />&nbsp;".$status_arr[$sorted_names[$i]]."</span></marquee></span>";
			
			else
			{
					echo "<br/><span style='font-size:11px;color:#990000;text-transform:lowercase' title='".sanitize($status_arr[$sorted_names[$i]],0)."'><i>".sanitize($status_arr[$sorted_names[$i]],1)."</i></span></span>";
			

			}
		}

		echo "</span><br/>";
		$i++;
	}
	echo "</div>";

}

function daily_report($dbcon,$dbRj,$del)
{
	$app_id="channeli";
	$query_select="select username from online_users where app_id='$app_id' order by username ";
	$select_result=pg_query($dbcon,$query_select);
	$users_cnt=pg_num_rows($select_result);
	$today = date("F j, Y, g:i a");                 
	$report="";
	$report.="$users_cnt unique visitors : Channel I on $today<hr/>";
	while($row=pg_fetch_row($select_result))
	{
		$report.= "<span id='text'>".$row[0]. "</span><br/>";
		

	}
	$rep_log="Reports/Report_".date("m_d_y").".html";
	$fh = fopen($rep_log, 'w');
	fwrite($fh, $report);
	fclose($fh);
	echo $report;
	if ($del==1)
	{
		$query_delete="delete from online_users ";
		$select_result=pg_query($dbcon,$query_delete);

	}

}


if($mode=="insert")
	make_log($dbcon,$user,$app_id);
else if($mode=="retrieve")
	{	
		if($app_id=="channeli")
		{
			make_log($dbcon,$user,$app_id);

		}
		get_online_users($app_id,$dbcon,$dbRj,$user,$sid);	
	
	}
else if($mode=="daily")
	daily_report($dbcon,$dbRj,$del);


pg_close($dbcon);
pg_close($dbRj);

?>
</body>
</html>
