<style type="text/css">
<!--
body{
	font-family:verdana;
	font-weight:900;
	font-size:11px;
}
#txt{

	font-family:verdana;
	font-size:11px;
	border-bottom:1px groove #444444;
	float:left;
	width:100%;
	cursor:pointer;
	font-weight:normal;

}
td,th{

font-size:11px;

}


-->
</style>
<script language="javascript" src="../status.js"></script>
<?php
error_reporting(1);
/*****************************
Purpose: Shows online users, generates daily usage report

***************************/

include("../../connectionFile.php");
$dbcon=getConnection($db_name,$db_user);
$dbRj=getConnection($db_regol,$db_user);

echo "<body>";


/******************************************************************************************************************************************/
                 /**********selecting app_id**********/
        $app_id_arr=array();		 
	$query_appid="select distinct(app_id) from online_users";
	$query_result=pg_query($dbcon,$query_appid);
        while($row=pg_fetch_row($query_result))
	{
	 $app_id=$row[0];
	 array_push($app_id_arr,$app_id);
	}
                 /*************************************/
        $cnt_arr=array();
        for($i=0;$i<sizeof($app_id_arr);$i++) 
        { 
	$array=array("$app_id_arr[$i]"=>array());
	$cnt_arr=array_merge($cnt_arr,$array);
        }
               /************************************/


function get_branch($dbcon,$dbRj,$app_id_arr,$cnt_arr)
{
	$branch_arr=array();

		 /***********selecting discipline & branch**************/
	$query_discipline="select distinct(discipline) from person_view";
	$discipline_result=pg_query($dbRj,$query_discipline);
	while($row=pg_fetch_row($discipline_result))
	{
	 $query_branch="select corresponds_to from codes_used where code='$row[0]'";
	 $branch_result=pg_query($dbRj,$query_branch);
	 $branch=pg_fetch_row($branch_result);
	 if($branch!=NULL) array_push($branch_arr,$branch[0]);
        }
	       /***********************************************************/

	$cnt=1;
     for($i=0;$i<sizeof($app_id_arr);$i++)
     {
        $query_select="select username from online_users where app_id='$app_id_arr[$i]'";
	$select_result=pg_query($dbcon,$query_select);
      
       for($j=0;$j<sizeof($branch_arr);$j++)
	{
	 $cnt_arr[$app_id_arr[$i]][$j]=0;
	}
    	
	while($row=pg_fetch_row($select_result))
      {
	$person_id=$row[0];
	$query_discipline="select discipline from person_view where person_id='$person_id'";
        $query_result=pg_query($dbRj,$query_discipline);

	 $discipline=pg_fetch_row($query_result);
	 $query_branch="select corresponds_to from codes_used where code='$discipline[0]'";
	 $branch_result=pg_query($dbRj,$query_branch);
	 $branch=pg_fetch_row($branch_result);
	for($j=0;$j<sizeof($branch_arr);$j++)
	{
           if($branch[0]==$branch_arr[$j])
           {
	   $cnt_arr[$app_id_arr[$i]][$j]+=$cnt;
           }
	}
        
     }
     }
      include('branch_report.php');
      return $report_branch;
} 

function get_visitors_yearwise($dbcon,$dbRj,$app_id_arr,$cnt_arr)
{
     $course_arr=array();

                 /*************************************/

     $query_course="select distinct(course) from person_view where course<300";
     $course_result=pg_query($dbRj,$query_course);
 
      while($row=pg_fetch_row($course_result))
     {
      $query_category="select corresponds_to from codes_used where code='$row[0]'";
      $category_result=pg_query($dbRj,$query_category);
      $category=pg_fetch_row($category_result);
      if($category[0]!==NULL) { array_push($course_arr,$category[0]);}
     } 
              /*******************************************/
	$cnt=1;
	for($i=0;$i<sizeof($app_id_arr);$i++)
      {
        $query_select="select username from online_users where app_id='$app_id_arr[$i]'";
	$select_result=pg_query($dbcon,$query_select);
      
        for($j=0;$j<sizeof($branch_arr);$j++)
	{
	 $cnt_arr[$app_id_arr[$i]][$j]=0;
	}       	
	while($row=pg_fetch_row($select_result))
      {
	$person_id=$row[0];
	$query_course="select course from person_view where person_id='$person_id'";
        $query_result=pg_query($dbRj,$query_course);

	 $course=pg_fetch_row($query_result);
	 $query_category="select corresponds_to from codes_used where code='$course[0]'";
	 $category_result=pg_query($dbRj,$query_category);
	 $category=pg_fetch_row($category_result);
	for($j=0;$j<sizeof($course_arr);$j++)
	{
        if($category[0]==$course_arr[$j])
	{
	 $cnt_arr[$app_id_arr[$i]][$j]+=$cnt;
        }
	}
        
      }
      }
     
     include('yearwise_report.php');
     return $report_yearwise;  
}

function click_report($dbcon)
{
     $link=array();   
     $link_cnt=array();
     $query_select="select distinct(link_name) from user_link_log";
     $select_result=pg_query($dbcon,$query_select);
     while($row=pg_fetch_row($select_result))
     {
       array_push($link,$row[0]);
     }

     $date= date("d",mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
     $month= date("m",mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")));
     for($i=0;$i<sizeof($link);$i++)
     {
       $query_count="select count(*) from user_link_log where date='$date' and month='$month' and link_name='$link[$i]'";
       $count_result=pg_query($dbcon,$query_count);
       $count=pg_fetch_row($count_result);
       array_push($link_cnt,$count[0]);
     }

      for($i=0;$i<(sizeof($link)-1);$i++)
       {
	  for($j=0;$j<(sizeof($link)-1-$i);$j++)
	   {
            if($link_cnt[$j+1]>$link_cnt[$j])
	     { 
	       $temp1=$link_cnt[$j];
	       $link_cnt[$j]=$link_cnt[$j+1];
	       $link_cnt[$j+1]=$temp1;

	       $temp2=$link[$j];
	       $link[$j]=$link[$j+1];
               $link[$j+1]=$temp2;
	     }	
	   }
        }   

     $table="";
     $table.= "<hr/><table border='1'><a name='click_log'><caption>Click Log on ".$date."-".$month."</caption></a><tr><th>Link Name</th><td>No. of Clicks</td></tr>";
     for($i=0;$i<sizeof($link);$i++)
     {
     $table.="<tr><td>".$link[$i]."</td><td>".$link_cnt[$i]."</td></tr>";
     }
     $table.="</table>";
     return $table;

}
function daily_report($dbcon,$dbRj,$del,$app_id_arr,$cnt_arr)
{
                   	/****************************************/
        $report="";          
	$today = date("F j, Y, g:i a");  
        $report.= "<a name='top'><hr/></a>";

	for($i=0;$i<sizeof($app_id_arr);$i++)
	{
	$query_select="select username from online_users where app_id='$app_id_arr[$i]'";
        $select_result=pg_query($dbcon,$query_select);
	$cnt_arr[$app_id_arr[$i]][100]=pg_num_rows($select_result); // 100 any no. greater than no. of branch
        $report.= $cnt_arr[$app_id_arr[$i]][100]." unique visitors: ".$app_id_arr[$i].", ";
        }

	$report.= "  on ".$today."<hr/>";
        $report.="<p><a href='#click_log'>See Click Report</a></p>";  
	$report.="<p><a href='#branch_report'>See Branchwise Report</a></p>";
        
	             /*************************************************/

	$report.=get_visitors_yearwise($dbcon,$dbRj,$app_id_arr,$cnt_arr);
	$report.=get_branch($dbcon,$dbRj,$app_id_arr,$cnt_arr);
        $report.=click_report($dbcon);
	$report.="<p align='center'><a href='#top'>Go to top</a></p>";
                    /*************************************************/
		
        $rep_log="../Reports/Report_".date("m_d_y").".html";
	$fh = fopen($rep_log, 'a');
	fwrite($fh, $report);
	fclose($fh);
	$query_delete="delete from online_users ";
	$select_result=pg_query($dbcon,$query_delete);
}


daily_report($dbcon,$dbRj,"1",$app_id_arr,$cnt_arr);
pg_close($dbcon);
pg_close($dbRj);

?>
</body>
</html>

